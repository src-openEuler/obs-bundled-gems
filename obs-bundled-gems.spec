#
# spec file for package obs-bundled-gems
#
# Copyright (c) 2018 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#

%global rake_version 13.0.6
%global rack_version 2.2.8
%global ruby_version 3.2.0
Name:           obs-bundled-gems
Version:        2.10.15
Release:        11
Summary:        The Open Build Service -- Bundled Gems
License:        GPL-2.0-only OR GPL-3.0-only
URL:            http://www.openbuildservice.org
Source0:        Gemfile
Source1:        Gemfile.lock
Source2:	verdor.obscpio

# https://rubygems.loongnix.cn/gems/ffi-1.15.5.gem 
Source5:	ffi-1.15.5.gem
BuildRequires:  cyrus-sasl-devel nodejs openldap-devel python3-devel
BuildRequires:  pkgconfig(mysqlclient) > 21.2.33
BuildRequires:  gcc gcc-c++ glibc-devel libtool libxml2-devel libxslt-devel
BuildRequires:  ruby-devel rubygem-bundler ruby-common make

%description
This package is useful for the Open Build Service. It will figure out
denpendent gems, and add them to spec file.

%package -n     obs-api-deps
Summary:        Holding dependencies required to run the OBS frontend
Requires:       build >= 20170315
Requires:       memcached >= 1.4
Requires:       mysql-server
Requires:       obs-bundled-gems = %{version}
Requires:       sphinx >= 2.1.8
Requires:       perl(GD)
Requires:       rubygem-bundler
Requires:       rubygem-rake

%description -n obs-api-deps
To simplify splitting the test suite packages off the main package,
this package is just a meta package used to run and build obs-api

%package -n obs-api-testsuite-deps
Summary:        Holding dependencies required to run frontend test suites
Group:          Productivity/Networking/Web/Utilities
Requires:       inst-source-utils
Requires:       nodejs
Requires:       obs-api-deps = %{version}

%description -n obs-api-testsuite-deps
To simplify splitting the test suite packages off the main package,
this package is just a meta package used to build obs-api testsuite

%prep
echo "This package contains of requirements"> README
cp %{_sourcedir}/Gemfile %{_sourcedir}/Gemfile.lock .


%build
cd %{_sourcedir}
if [ ! -f verdor.obscpio ]; then find ./vendor/cache | cpio -o -H newc > %{_sourcedir}/verdor.obscpio; fi
if [ -d vendor ]; then rm -rf vendor; fi
cpio -idvm < verdor.obscpio
cd -

arch=`uname -m`
if [ $arch == 'loongarch64' ];then
  cp %{SOURCE5} %{_sourcedir}/vendor/cache/
fi

export QA_SKIP_BUILD_ROOT=1
# copy gem files into cache
mkdir -p vendor/cache
cp %{_sourcedir}/vendor/cache/*.gem vendor/cache
export GEM_HOME=~/.gems
bundle config build.nokogiri --use-system-libraries

%install
export QA_SKIP_BUILD_ROOT=1
bundle config set --local path %{buildroot}%{_libdir}/obs-api/
bundle --local

# test that the rake and rack macros is still matching our Gemfile
test -f %{buildroot}%_libdir/obs-api/ruby/%{ruby_version}/gems/rake-%{rake_version}/rake.gemspec
test -f %{buildroot}%_libdir/obs-api/ruby/%{ruby_version}/gems/rack-%{rack_version}/rack.gemspec

# run gem clean up script
/usr/lib/rpm/gem_build_cleanup.sh %{buildroot}%_libdir/obs-api/ruby/*/

# work around sassc bug - and install libsass
sassc_dir=$(ls -1d %{buildroot}%_libdir/obs-api/ruby/%{ruby_version}/gems/sassc-2*)
libpath=`find %{buildroot} -name libsass.so`
install -D -m 755 $libpath $sassc_dir/lib/sassc
sed -i -e 's,/ext/libsass,,' $sassc_dir/lib/sassc/native.rb

# Remove sources of extensions, we don't need them
rm -rf %{buildroot}%_libdir/obs-api/ruby/*/gems/*/ext/

# remove binaries with invalid interpreters
rm -rf %{buildroot}%_libdir/obs-api/ruby/*/gems/diff-lcs-*/bin

# remove spec / test files from gems as they shouldn't be shipped in gems anyway
# and often cause errors / warning in rpmlint
rm -rf %{buildroot}%_libdir/obs-api/ruby/*/gems/*/spec/
rm -rf %{buildroot}%_libdir/obs-api/ruby/*/gems/*/test/
# we do not verify signing of the gem
rm -rf %{buildroot}%_libdir/obs-api/ruby/*/gems/mousetrap-rails-*/gem-public_cert.pem

# remove prebuilt binaries causing broken dependencies
rm -rf %{buildroot}%_libdir/obs-api/ruby/*/gems/selenium-webdriver-*/lib/selenium/webdriver/firefox/native

# remove all gitignore files to fix rpmlint version-control-internal-file
find %{buildroot}%_libdir/obs-api -name .gitignore | xargs rm -rf

# fix interpreter in installed binaries
for bin in %{buildroot}%_libdir/obs-api/ruby/*/bin/*; do
  sed -i -e 's,/usr/bin/env ruby.ruby2.5,/usr/bin/ruby.ruby2.5,' $bin
done

# remove exec bit from all other files still containing /usr/bin/env except ruby/2.5.0/bin - mostly helper scripts
find %{buildroot} -type f -print0 | xargs -0 grep -l /usr/bin/env | grep -v ruby/2.5.0/bin| while read file; do
  chmod a-x $file
done

%files
%_libdir/obs-api

%files -n obs-api-deps
%doc README

%files -n obs-api-testsuite-deps
%doc README

%changelog
* Wed Feb 26 2025 wangkai <13474090681@163.com> - 2.10.15-11
- Replace mariadb-server with mysql-server

* Thu Feb 20 2025 wangkai <13474090681@163.com> - 2.10.15-10
- Replace ruby-ldap with ruby-ldap3 to fix service failed

* Wed May 15 2024 Wenlong Zhang<zhangwenlong@loongson.cn> - 2.10.15-9
- add loongarch64 support for obs-bundled-gems

* Tue Aug 29 2023 liyanan <thistleslyn@163.com> - 2.10.15-8
- Replace Gemfile with commit de86991072b9457dd7d12dee129aef8326829935 

* Sat Aug 12 2023 Ge Wang <wang__ge@126.com> 2.10.15-7
- Fix obs-server build failure due to ruby updated to version 3.2

* Tue Aug 08 2023 Ge Wang <wang__ge@126.com> 2.10.15-6
- adapt package ruby-ldap due to ruby updated to version 3.2

* Sat Apr 1 2023 Wenlong Zhang <zhangwenlong@loongson.cn> 2.10.16-5
- add loongarch64 support for obs-bundle-gems

* Fri Mar 17 2023 misaka00251 <liuxin@iscas.ac.cn> 2.10.15-4
- Fix build on riscv64

* Mon Feb 13 2023 wangkai <wangkai385@h-partners.com> 2.10.15-3
- Modify the rdoc version to 6.3.3

* Wed Feb 08 2023 wangkai <wangkai385@h-partners.com> 2.10.15-2
- Switch bundled version to 2.3.26
- Add stringio-3.0.1.gem to verdor.obscpio

* Tue Jan 17 2023 Ge Wang <wangge20@h-partners.com> 2.10.15-1
- Fix build failure due tu ruby updated to version 3.1.3
- Modify version to match version of obs-server

* Mon May 23 2022 wangkai <wangkai385@h-partners.com> 2.10.1-6
- Update gem version for obs-server 2.10.15

* Thu May 12 2022 wangkai <wangkai385@h-partners.com> 2.10.1-5
- Update xmlhash to 1.3.8

* Mon Mar 21 2022 Ge Wang <wangge20@huawei.com> 2.10.1-4
- Fix compilation failed due to ruby and some rubygems upgraded

* Thu Sep 23 2021 lingsheng <lingsheng@huawei.com> 2.10.1-3
- Fix exec permission in ruby/2.5.0/bin, change require to mariadb-server

* Wed Jun 9 2021 zhaomengchao <zhaomengchao3@huawei.com> 2.10.1-2
- fix obs-bundled-gems compilation failed.

* Tue Nov 17 2020 yanglongkang <yanglongkang@huawei.com> 2.10.1-1
- Add verdor.obscpio to the source code package.

* Sat Mar 7 2020 Shijie Luo <luoshijie1@huawei.com> 2.10.1-0
- Package Init.
